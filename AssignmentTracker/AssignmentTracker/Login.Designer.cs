﻿namespace AssignmentTracker
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.buttonExit = new System.Windows.Forms.Button();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelIsmtFullForm = new System.Windows.Forms.Label();
            this.labelIsmt = new System.Windows.Forms.Label();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.pictureBoxImage = new System.Windows.Forms.PictureBox();
            this.groupBoxLogin = new System.Windows.Forms.GroupBox();
            this.buttonLogIn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.labelUserName = new System.Windows.Forms.Label();
            this.CheckboxShowHidePassword = new System.Windows.Forms.CheckBox();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textboxUserName = new System.Windows.Forms.TextBox();
            this.labelDate = new System.Windows.Forms.Label();
            this.labelTime = new System.Windows.Forms.Label();
            this.buttonFacebook = new System.Windows.Forms.Button();
            this.buttonMail = new System.Windows.Forms.Button();
            this.time = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).BeginInit();
            this.groupBoxLogin.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonExit
            // 
            this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExit.BackgroundImage = global::AssignmentTracker.Properties.Resources.shutDown512;
            this.buttonExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Location = new System.Drawing.Point(1030, 12);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(81, 78);
            this.buttonExit.TabIndex = 0;
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // labelCopyright
            // 
            this.labelCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCopyright.ForeColor = System.Drawing.Color.White;
            this.labelCopyright.Location = new System.Drawing.Point(12, 528);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(649, 37);
            this.labelCopyright.TabIndex = 10;
            this.labelCopyright.Text = "© Copyright 2019. All rights reserved By ISMT";
            // 
            // labelIsmtFullForm
            // 
            this.labelIsmtFullForm.AutoSize = true;
            this.labelIsmtFullForm.Font = new System.Drawing.Font("Monotype Corsiva", 19.875F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIsmtFullForm.ForeColor = System.Drawing.Color.White;
            this.labelIsmtFullForm.Location = new System.Drawing.Point(485, 96);
            this.labelIsmtFullForm.Name = "labelIsmtFullForm";
            this.labelIsmtFullForm.Size = new System.Drawing.Size(572, 128);
            this.labelIsmtFullForm.TabIndex = 12;
            this.labelIsmtFullForm.Text = "     International School of \r\nManagement and Technology";
            // 
            // labelIsmt
            // 
            this.labelIsmt.AutoSize = true;
            this.labelIsmt.Font = new System.Drawing.Font("Cooper Black", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelIsmt.ForeColor = System.Drawing.Color.White;
            this.labelIsmt.Location = new System.Drawing.Point(521, -5);
            this.labelIsmt.Name = "labelIsmt";
            this.labelIsmt.Size = new System.Drawing.Size(527, 220);
            this.labelIsmt.TabIndex = 13;
            this.labelIsmt.Text = "ismt";
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Image = global::AssignmentTracker.Properties.Resources.IsmtLogo_White;
            this.pictureBoxLogo.Location = new System.Drawing.Point(12, 166);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(396, 300);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLogo.TabIndex = 14;
            this.pictureBoxLogo.TabStop = false;
            // 
            // pictureBoxImage
            // 
            this.pictureBoxImage.Image = global::AssignmentTracker.Properties.Resources.cloud;
            this.pictureBoxImage.Location = new System.Drawing.Point(874, 218);
            this.pictureBoxImage.Name = "pictureBoxImage";
            this.pictureBoxImage.Size = new System.Drawing.Size(237, 215);
            this.pictureBoxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxImage.TabIndex = 16;
            this.pictureBoxImage.TabStop = false;
            // 
            // groupBoxLogin
            // 
            this.groupBoxLogin.Controls.Add(this.buttonLogIn);
            this.groupBoxLogin.Controls.Add(this.label5);
            this.groupBoxLogin.Controls.Add(this.labelUserName);
            this.groupBoxLogin.Controls.Add(this.CheckboxShowHidePassword);
            this.groupBoxLogin.Controls.Add(this.textBoxPassword);
            this.groupBoxLogin.Controls.Add(this.textboxUserName);
            this.groupBoxLogin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxLogin.ForeColor = System.Drawing.Color.White;
            this.groupBoxLogin.Location = new System.Drawing.Point(442, 209);
            this.groupBoxLogin.MaximumSize = new System.Drawing.Size(410, 284);
            this.groupBoxLogin.Name = "groupBoxLogin";
            this.groupBoxLogin.Size = new System.Drawing.Size(410, 284);
            this.groupBoxLogin.TabIndex = 17;
            this.groupBoxLogin.TabStop = false;
            this.groupBoxLogin.Text = "User Login";
            // 
            // buttonLogIn
            // 
            this.buttonLogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogIn.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.buttonLogIn.Location = new System.Drawing.Point(116, 220);
            this.buttonLogIn.Name = "buttonLogIn";
            this.buttonLogIn.Size = new System.Drawing.Size(177, 47);
            this.buttonLogIn.TabIndex = 8;
            this.buttonLogIn.Text = "Login";
            this.buttonLogIn.UseVisualStyleBackColor = true;
            this.buttonLogIn.Click += new System.EventHandler(this.buttonLogIn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.label5.Location = new System.Drawing.Point(14, 125);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(153, 37);
            this.label5.TabIndex = 7;
            this.label5.Text = "Password";
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Font = new System.Drawing.Font("Century Gothic", 11.25F);
            this.labelUserName.Location = new System.Drawing.Point(14, 40);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(166, 37);
            this.labelUserName.TabIndex = 6;
            this.labelUserName.Text = "UserName";
            // 
            // CheckboxShowHidePassword
            // 
            this.CheckboxShowHidePassword.Appearance = System.Windows.Forms.Appearance.Button;
            this.CheckboxShowHidePassword.AutoSize = true;
            this.CheckboxShowHidePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CheckboxShowHidePassword.ForeColor = System.Drawing.Color.White;
            this.CheckboxShowHidePassword.Image = ((System.Drawing.Image)(resources.GetObject("CheckboxShowHidePassword.Image")));
            this.CheckboxShowHidePassword.Location = new System.Drawing.Point(350, 153);
            this.CheckboxShowHidePassword.Name = "CheckboxShowHidePassword";
            this.CheckboxShowHidePassword.Size = new System.Drawing.Size(31, 31);
            this.CheckboxShowHidePassword.TabIndex = 5;
            this.CheckboxShowHidePassword.UseVisualStyleBackColor = true;
            this.CheckboxShowHidePassword.CheckedChanged += new System.EventHandler(this.CheckboxShowHidePassword_CheckedChanged);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPassword.Location = new System.Drawing.Point(21, 153);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(323, 54);
            this.textBoxPassword.TabIndex = 2;
            this.textBoxPassword.UseSystemPasswordChar = true;
            this.textBoxPassword.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxPassword_KeyPress);
            // 
            // textboxUserName
            // 
            this.textboxUserName.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textboxUserName.Location = new System.Drawing.Point(21, 68);
            this.textboxUserName.Name = "textboxUserName";
            this.textboxUserName.Size = new System.Drawing.Size(360, 54);
            this.textboxUserName.TabIndex = 1;
            this.textboxUserName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textboxUserName_KeyPress);
            // 
            // labelDate
            // 
            this.labelDate.AutoSize = true;
            this.labelDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDate.ForeColor = System.Drawing.Color.White;
            this.labelDate.Location = new System.Drawing.Point(83, 10);
            this.labelDate.Name = "labelDate";
            this.labelDate.Size = new System.Drawing.Size(72, 31);
            this.labelDate.TabIndex = 18;
            this.labelDate.Text = "Date";
            // 
            // labelTime
            // 
            this.labelTime.AutoSize = true;
            this.labelTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTime.ForeColor = System.Drawing.Color.White;
            this.labelTime.Location = new System.Drawing.Point(68, 91);
            this.labelTime.Name = "labelTime";
            this.labelTime.Size = new System.Drawing.Size(74, 31);
            this.labelTime.TabIndex = 19;
            this.labelTime.Text = "Time";
            // 
            // buttonFacebook
            // 
            this.buttonFacebook.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFacebook.FlatAppearance.BorderSize = 0;
            this.buttonFacebook.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonFacebook.ForeColor = System.Drawing.Color.White;
            this.buttonFacebook.Image = ((System.Drawing.Image)(resources.GetObject("buttonFacebook.Image")));
            this.buttonFacebook.Location = new System.Drawing.Point(15, 34);
            this.buttonFacebook.Name = "buttonFacebook";
            this.buttonFacebook.Size = new System.Drawing.Size(62, 53);
            this.buttonFacebook.TabIndex = 20;
            this.buttonFacebook.UseVisualStyleBackColor = true;
            this.buttonFacebook.Click += new System.EventHandler(this.buttonFacebook_Click);
            // 
            // buttonMail
            // 
            this.buttonMail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonMail.FlatAppearance.BorderSize = 0;
            this.buttonMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMail.ForeColor = System.Drawing.Color.White;
            this.buttonMail.Image = ((System.Drawing.Image)(resources.GetObject("buttonMail.Image")));
            this.buttonMail.Location = new System.Drawing.Point(86, 34);
            this.buttonMail.Name = "buttonMail";
            this.buttonMail.Size = new System.Drawing.Size(68, 54);
            this.buttonMail.TabIndex = 21;
            this.buttonMail.UseVisualStyleBackColor = true;
            this.buttonMail.Click += new System.EventHandler(this.buttonMail_Click);
            // 
            // time
            // 
            this.time.Enabled = true;
            this.time.Tick += new System.EventHandler(this.time_Tick);
            // 
            // Login
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(70)))), ((int)(((byte)(160)))));
            this.BackgroundImage = global::AssignmentTracker.Properties.Resources.computer;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1123, 574);
            this.Controls.Add(this.buttonMail);
            this.Controls.Add(this.buttonFacebook);
            this.Controls.Add(this.labelTime);
            this.Controls.Add(this.labelDate);
            this.Controls.Add(this.groupBoxLogin);
            this.Controls.Add(this.pictureBoxImage);
            this.Controls.Add(this.pictureBoxLogo);
            this.Controls.Add(this.labelIsmtFullForm);
            this.Controls.Add(this.labelIsmt);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.buttonExit);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Login";
            this.Load += new System.EventHandler(this.Login_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Login_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Login_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Login_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxImage)).EndInit();
            this.groupBoxLogin.ResumeLayout(false);
            this.groupBoxLogin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelIsmtFullForm;
        private System.Windows.Forms.Label labelIsmt;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.PictureBox pictureBoxImage;
        private System.Windows.Forms.GroupBox groupBoxLogin;
        private System.Windows.Forms.Button buttonLogIn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.CheckBox CheckboxShowHidePassword;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textboxUserName;
        private System.Windows.Forms.Label labelDate;
        private System.Windows.Forms.Label labelTime;
        private System.Windows.Forms.Button buttonFacebook;
        private System.Windows.Forms.Button buttonMail;
        private System.Windows.Forms.Timer time;
    }
}

