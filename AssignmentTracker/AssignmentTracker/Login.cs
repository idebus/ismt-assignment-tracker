﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AssignmentTracker
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        private const string UserName = "admin";//username
        private const string Password = "admin";//password

        #region//Exit and login form dragable
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Login_MouseDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void Login_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        private void Login_MouseUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }
        #endregion
        //making form dragable and exit button ends

        private void Login_Load(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToLongTimeString();
            labelDate.Text = DateTime.Now.ToShortDateString();
            groupBoxLogin.Height = 123;
        }
        //Form Load Event Ends

        private void time_Tick(object sender, EventArgs e)
        {
            labelTime.Text = DateTime.Now.ToLongTimeString();
            labelDate.Text = DateTime.Now.ToShortDateString();
        }
        //TimeTick event Ends

        private void CheckboxShowHidePassword_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckboxShowHidePassword.Checked)
            {
                textBoxPassword.UseSystemPasswordChar = false;
            }
            else
            {
                textBoxPassword.UseSystemPasswordChar = true;
            }
        }
        //checkbox show hide password ends

        private void textboxUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)13) && textboxUserName.Text != "")
            {
                groupBoxLogin.Height = 214;
                textBoxPassword.Clear();
                textBoxPassword.Focus();
            }
            else
            {
                groupBoxLogin.Height = 123;
            }
        }
        //Textbox kep press enter event ends

        private void textBoxPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)13) && textBoxPassword.Text != "")
            {
                groupBoxLogin.Height = 284;
                buttonLogIn.Focus();
            }
            else
            {
                groupBoxLogin.Height = 214;
            }
        }
        //password kep press enter event ends

        private void buttonFacebook_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/ISMTCollege/");
        }
        // button facebook  click event ends

        private void buttonMail_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://mail.google.com");
        }
        // button mail  click event ends

        private void buttonLogIn_Click(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            if (textboxUserName.Text == UserName)
            {
                if (textBoxPassword.Text == Password)
                {
                    MessageBox.Show("Login success");
                    main.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Incorect Password");
                    textBoxPassword.Focus();
                    textBoxPassword.Clear();
                    groupBoxLogin.Height = 214;
                }
            }
            else
            {
                MessageBox.Show("Incorect User Name");
                textboxUserName.Focus();
                textboxUserName.Clear();
                textBoxPassword.Clear();
                groupBoxLogin.Height = 123;
            }
        }
        // button login  click event ends
    }
}
