﻿using AssignmentTracker.Properties;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace AssignmentTracker
{
    public partial class MainForm : Form
    {
        #region//Dragable 

        #region//Variable for making main form dragable
        private bool _Dragging = false;
        private Point _start_point = new Point(0, 0);
        #endregion

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (_Dragging)
            {
                Point p = PointToScreen(e.Location);
                Location = new Point(p.X - this._start_point.X, p.Y - this._start_point.Y);
            }
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            _Dragging = true;
            _start_point = new Point(e.X, e.Y);
        }

        private void MainForm_MouseUp(object sender, MouseEventArgs e)
        {
            _Dragging = false;
        }

        #endregion
       //Making form Dragable ends

        public MainForm()
        {
            InitializeComponent();
        }

        private bool sliderIsEnabled = false;
        private void MainForm_Load(object sender, EventArgs e)
        {
            panelRightSlider.Width = 0;
            buttonMaximize.BackgroundImage = Resources.Extender;
            buttonSlider.BackgroundImage = Resources.sliderright;
            buttonStudentsProfile.Image = null;
        }
        //form load event ends

        #region//MAximize Minimize and Exit
        private void buttonExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonMaximize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                buttonMaximize.BackgroundImage = Resources.Srink;
                WindowState = FormWindowState.Maximized;
                panelRightSlider.Width = 0;
                sliderIsEnabled = false;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                buttonMaximize.BackgroundImage = Resources.Extender;
                WindowState = FormWindowState.Normal;
                panelRightSlider.Width = 0;
                sliderIsEnabled = false;
            }
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                WindowState = FormWindowState.Minimized;
                sliderIsEnabled = false;
            }
            else if (WindowState == FormWindowState.Maximized)
            {
                WindowState = FormWindowState.Minimized;
                sliderIsEnabled = false;
            }
         
        }

        #endregion
        //maximize , minimize and exit ends

        private void panelRight_Click(object sender, EventArgs e)
        {
            panelRightSlider.Width = 0;
            sliderIsEnabled = false;
            buttonSlider.BackgroundImage = Resources.sliderright;
        }
        //pannel right click ends

        private void MainForm_Click(object sender, EventArgs e)
        {
            panelRightSlider.Width = 0;
            sliderIsEnabled = false;
            buttonSlider.BackgroundImage = Resources.sliderright;
        }
        //form click ends

        private void buttonSlider_Click(object sender, EventArgs e)
        {
            if (!sliderIsEnabled)
            {
                buttonSlider.BackgroundImage = Resources.sliderLeft;
                if ((WindowState == FormWindowState.Maximized))
                {
                    buttonStudentsProfile.Image = Resources.student;
                    panelRightSlider.Width = 260;
                }
                else if ((WindowState == FormWindowState.Normal))
                {
                    buttonStudentsProfile.Image = null;
                    panelRightSlider.Width = 90;
                }
                sliderIsEnabled = true;
            }
            else if (sliderIsEnabled)
            {
                buttonSlider.BackgroundImage = Resources.sliderright;
                panelRightSlider.Width = 0;
                sliderIsEnabled = false;
            }
        }
        //buttonslider click ends
    }
}
