﻿namespace AssignmentTracker
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelRight = new System.Windows.Forms.Panel();
            this.buttonSlider = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonMaximize = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.pictureBoxIsmtlogo = new System.Windows.Forms.PictureBox();
            this.panelRightSlider = new System.Windows.Forms.Panel();
            this.buttonStudentsProfile = new System.Windows.Forms.Button();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIsmtlogo)).BeginInit();
            this.panelRightSlider.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelRight
            // 
            this.panelRight.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(14)))), ((int)(((byte)(70)))), ((int)(((byte)(160)))));
            this.panelRight.Controls.Add(this.buttonSlider);
            this.panelRight.Controls.Add(this.buttonMinimize);
            this.panelRight.Controls.Add(this.buttonMaximize);
            this.panelRight.Controls.Add(this.buttonExit);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRight.Location = new System.Drawing.Point(1453, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(85, 833);
            this.panelRight.TabIndex = 1;
            this.panelRight.Click += new System.EventHandler(this.panelRight_Click);
            // 
            // buttonSlider
            // 
            this.buttonSlider.BackgroundImage = global::AssignmentTracker.Properties.Resources.sliderright;
            this.buttonSlider.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.buttonSlider.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonSlider.FlatAppearance.BorderSize = 0;
            this.buttonSlider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSlider.Location = new System.Drawing.Point(0, 239);
            this.buttonSlider.Name = "buttonSlider";
            this.buttonSlider.Size = new System.Drawing.Size(85, 69);
            this.buttonSlider.TabIndex = 14;
            this.buttonSlider.UseVisualStyleBackColor = true;
            this.buttonSlider.Click += new System.EventHandler(this.buttonSlider_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackgroundImage = global::AssignmentTracker.Properties.Resources.Minimize;
            this.buttonMinimize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMinimize.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonMinimize.FlatAppearance.BorderSize = 0;
            this.buttonMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMinimize.Location = new System.Drawing.Point(0, 156);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(85, 83);
            this.buttonMinimize.TabIndex = 13;
            this.buttonMinimize.UseVisualStyleBackColor = true;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonMaximize
            // 
            this.buttonMaximize.BackgroundImage = global::AssignmentTracker.Properties.Resources.Extender;
            this.buttonMaximize.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonMaximize.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonMaximize.FlatAppearance.BorderSize = 0;
            this.buttonMaximize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMaximize.Location = new System.Drawing.Point(0, 78);
            this.buttonMaximize.Name = "buttonMaximize";
            this.buttonMaximize.Size = new System.Drawing.Size(85, 78);
            this.buttonMaximize.TabIndex = 12;
            this.buttonMaximize.UseVisualStyleBackColor = true;
            this.buttonMaximize.Click += new System.EventHandler(this.buttonMaximize_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.BackgroundImage = global::AssignmentTracker.Properties.Resources.shutDown512;
            this.buttonExit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonExit.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonExit.FlatAppearance.BorderSize = 0;
            this.buttonExit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonExit.Location = new System.Drawing.Point(0, 0);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(85, 78);
            this.buttonExit.TabIndex = 11;
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // pictureBoxIsmtlogo
            // 
            this.pictureBoxIsmtlogo.Cursor = System.Windows.Forms.Cursors.Default;
            this.pictureBoxIsmtlogo.Image = global::AssignmentTracker.Properties.Resources.IsmtLogo;
            this.pictureBoxIsmtlogo.Location = new System.Drawing.Point(12, 12);
            this.pictureBoxIsmtlogo.Name = "pictureBoxIsmtlogo";
            this.pictureBoxIsmtlogo.Size = new System.Drawing.Size(312, 212);
            this.pictureBoxIsmtlogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxIsmtlogo.TabIndex = 11;
            this.pictureBoxIsmtlogo.TabStop = false;
            // 
            // panelRightSlider
            // 
            this.panelRightSlider.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panelRightSlider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(8)))), ((int)(((byte)(85)))), ((int)(((byte)(209)))));
            this.panelRightSlider.Controls.Add(this.buttonStudentsProfile);
            this.panelRightSlider.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelRightSlider.Location = new System.Drawing.Point(1139, 0);
            this.panelRightSlider.Name = "panelRightSlider";
            this.panelRightSlider.Size = new System.Drawing.Size(314, 833);
            this.panelRightSlider.TabIndex = 12;
            // 
            // buttonStudentsProfile
            // 
            this.buttonStudentsProfile.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonStudentsProfile.FlatAppearance.BorderSize = 0;
            this.buttonStudentsProfile.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.buttonStudentsProfile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStudentsProfile.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStudentsProfile.ForeColor = System.Drawing.Color.White;
            this.buttonStudentsProfile.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonStudentsProfile.Location = new System.Drawing.Point(0, 0);
            this.buttonStudentsProfile.Margin = new System.Windows.Forms.Padding(6);
            this.buttonStudentsProfile.Name = "buttonStudentsProfile";
            this.buttonStudentsProfile.Size = new System.Drawing.Size(314, 103);
            this.buttonStudentsProfile.TabIndex = 2;
            this.buttonStudentsProfile.Text = " Student Details";
            this.buttonStudentsProfile.UseVisualStyleBackColor = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::AssignmentTracker.Properties.Resources.workstation;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1538, 833);
            this.Controls.Add(this.panelRightSlider);
            this.Controls.Add(this.pictureBoxIsmtlogo);
            this.Controls.Add(this.panelRight);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Click += new System.EventHandler(this.MainForm_Click);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseUp);
            this.panelRight.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxIsmtlogo)).EndInit();
            this.panelRightSlider.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Button buttonExit;
        private System.Windows.Forms.Button buttonMaximize;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.PictureBox pictureBoxIsmtlogo;
        private System.Windows.Forms.Panel panelRightSlider;
        private System.Windows.Forms.Button buttonStudentsProfile;
        private System.Windows.Forms.Button buttonSlider;
    }
}